#!/bin/bash
set -xeuo pipefail
BASE_DIR=$(cd "$(dirname "$0")"; pwd)
cd ${BASE_DIR}
# ROOT用户

# vscode的安装
# vscode https://code.visualstudio.com/docs/setup/linux
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
rm -f packages.microsoft.gpg 

sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

apt-get install -y apt-transport-https
apt-get update
apt-get install -y code # or code-insiders

###############
#vscode在root下启动，指定默认目录
mkdir -p /opt/vscode
echo "/usr/bin/code --user-data-dir /opt/vscode" > /usr/bin/vscode
chmod +x /usr/bin/vscode

# vscode的extension
#Visual Studio Code plugin that autocompletes npm modules in import statements
code --user-data-dir /opt/vscode --install-extension christian-kohler.npm-intellisense
# Bracket Pair Colorizer
code --user-data-dir /opt/vscode --install-extension CoenraadS.bracket-pair-colorizer

code --user-data-dir /opt/vscode --install-extension donjayamanne.githistory
code --user-data-dir /opt/vscode --install-extension eamodio.gitlens
#Prettier - Code formatter
code --user-data-dir /opt/vscode --install-extension esbenp.prettier-vscode
#输入时有特效，好玩
code --user-data-dir /opt/vscode --install-extension hoovercj.vscode-power-mode
# docker docker-compose使用超方便
code --user-data-dir /opt/vscode --install-extension ms-azuretools.vscode-docker
#VUE
code --user-data-dir /opt/vscode --install-extension octref.vetur
code --user-data-dir /opt/vscode --install-extension mhutchie.git-graph
code --user-data-dir /opt/vscode --install-extension sdras.vue-vscode-extensionpack
code --user-data-dir /opt/vscode --install-extension sdras.vue-vscode-snippets
code --user-data-dir /opt/vscode --install-extension xabikos.JavaScriptSnippets

code --user-data-dir /opt/vscode --install-extension tombonnike.vscode-status-bar-format-toggle
code --user-data-dir /opt/vscode --install-extension wmaurer.vscode-jumpy

code --user-data-dir /opt/vscode --install-extension xykong.format-all-files
code --user-data-dir /opt/vscode --install-extension xyz.local-history

code --user-data-dir /opt/vscode --install-extension vscode-icons-team.vscode-icons
code --user-data-dir /opt/vscode --install-extension dbaeumer.vscode-eslint
code --user-data-dir /opt/vscode --install-extension andrew-codes.cypress-snippets
code --user-data-dir /opt/vscode --install-extension marcostazi.vs-code-vagrantfile
# json - yaml的转换工具
code --user-data-dir /opt/vscode --install-extension ahebrank.yaml2json

code --user-data-dir /opt/vscode --install-extension asciidoctor.asciidoctor-vscode
code --user-data-dir /opt/vscode --install-extension uvdream.git-commit-lint-vscode
code --user-data-dir /opt/vscode --install-extension be5invis.toml
code --user-data-dir /opt/vscode --install-extension visualstudioexptteam.vscodeintellicode
echo "vscode extensions"

###############
# node相关的项
#npm config set registry https://registry.npm.taobao.org
#NEXUS=http://localhost:8083/repository/npm/
NEXUS=https://registry.npm.taobao.org/

npm config set registry ${NEXUS}
yarn config set registry ${NEXUS}

npm install -g typescript

echo "done"
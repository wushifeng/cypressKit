#!/bin/bash
set -xeuo pipefail
BASE_DIR=$(cd "$(dirname "$0")"; pwd)
cd ${BASE_DIR}

# Running headless  批量命令行模式

#共享主机的网卡，不用操心网络服务端口这些问题了
PARAM="--rm -v $PWD:/e2e -w /e2e --network host cypress/included:6.1.0"
DPARAM="-e DEBUG=cypress:* ${PARAM}"

if [ $# -eq 0 ]; then
 #默认运行
 docker run -it ${PARAM}
 
 #指定浏览器
 docker run -it ${PARAM} --browser chrome
 docker run -it ${PARAM} --browser firefox
else
#带有DEBUG信息输出
 docker run -it ${DPARAM} 
fi

echo "done"
#!/bin/bash
set -xeuo pipefail
BASE_DIR=$(cd "$(dirname "$0")"; pwd)
cd ${BASE_DIR}

:<<EOF
生成自己的cypress开发镜像[包含运行信息的模式生成的镜像]
在1个SHELL中
1 执行runui.sh 会看到弹出的窗口
cypress/included:6.1.0 是官方提供的包含了cypress和浏览器的镜像

在另一个SHELL中
2 docker exec -it cypressui bash下依次执行
 ./ide_install.sh 安装必要的工具
 vscode 启动vscode并设置自己的偏好，或者不修改使用默认
3 将如上修改形成镜像内部分发使用： 本文件做的事情
EOF
#Cypress 6.1.0
docker images| grep "cypress/included" ||(
    echo "cypress image not exist"
    docker pull cypress/included:6.1.0
)

./runui.sh
echo "cypressui... Wait"
sleep 5
echo "Install tools"
docker exec -it cypressui bash -c /e2e/ide_install.sh
#vscode的一些定制项
docker cp settings.json cypressui:/opt/vscode/User/settings.json
#如果vscode习惯还要定制，执行这个在界面上定制
#docker exec -it cypressui bash -c vscode

read -p "软件环境备好了吗？回车后，就会形成新的发布镜像了..."

#形成的镜像，其他的使用就基于这个镜像了
docker stop cypressui
docker commit cypressui dev.docker:8085/cypress-allinone:6.1.0
docker images|grep cypress-allinone

docker save dev.docker:8085/cypress-allinone:6.1.0 -o cypress-allinone_6.1.0.tar

echo "dev.docker:8085/cypress-allinone:6.1.0 这个镜像可以进行cypress的开发了"
echo "有docker环境的机器执行： docker load -i cypress-allinone_6.1.0.tar"
echo 
:<<EOF
导入后的机器上的类似如下：
docker images|grep cypress-allinone
 dev.docker:8085/cypress-allinone      6.1.0                          514ffeb697ee        20 hours ago        3.08GB
EOF
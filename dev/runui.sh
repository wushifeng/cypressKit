#!/bin/bash
set -xeuo pipefail
BASE_DIR=$(cd "$(dirname "$0")"; pwd)
cd ${BASE_DIR}
#交互模式运行

#可以查看当前支持的浏览器
# docker run -it --rm --entrypoint=cypress cypress/included:6.1.0 info

#默认electron中运行
docker rm cypressui -f || true

docker run -itd --name cypressui -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY -v $PWD:/e2e --network host -w /e2e --entrypoint=cypress cypress/included:6.1.0 open --project . --browser electron

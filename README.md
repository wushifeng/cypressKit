# Cypress docker 镜像

参考官方： https://github.com/cypress-io/cypress-docker-images

# 目录说明

- dev: 加入 vscode 等工具形成我们使用的镜像

```
执行的步骤参考ide.sh中的说明
这个镜像包含了： cypress chrome firefox vscode vscode插件
```

- starter: 一个简单的例子，可以使用 javascript 或者 typescript 来执行 e2e 测试

```
执行run.sh即可查看
```

- 官方发布真实的例子
  https://github.com/cypress-io/cypress-example-conduit-app
  https://github.com/cypress-io/cypress-realworld-app

```
https://github.com/cypress-io/cypress-realworld-app
run.sh运行后，刷新cypress可看结果
```

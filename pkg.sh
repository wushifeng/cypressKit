#!/bin/bash

#查看可以清理的文件
git clean -ndX

#确定好之后要清理运行
#docker root用户执行的，因此需要权限
sudo git clean -fdX

tar czvf ../cypressKit.tar.gz .

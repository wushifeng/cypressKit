use test;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ddm_account
-- ----------------------------
DROP TABLE IF EXISTS `ddm_account`;
CREATE TABLE `ddm_account` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ddm_account
-- ----------------------------
INSERT INTO `ddm_account` VALUES ('Admin', '7488e331b8b64e5794da3fa4eb10ad5d', 'Admin');
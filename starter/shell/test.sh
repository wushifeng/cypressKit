#!/bin/bash
WORK_DIR=$(cd `dirname $0`; pwd)
#
source ${WORK_DIR}/common.sh

${MYSQL} "
CREATE DATABASE IF NOT EXISTS test  DEFAULT CHARSET utf8mb4;
source $WORK_DIR/test.sql;
quit";

echo "this is test demo"

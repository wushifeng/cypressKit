//  速度最快的单个运行方式 npx cypress open -b electron

//测试需要调用脚本进行一些辅助初始化和后台的校验之类
context("Shell", () => {
  // exec for test
  it("exec", () => {
    cy.exec("sh shell/test.sh")
      .its("stdout")
      .then(($t) => {
        cy.log("shell run")
        cy.log($t)
      })
  })

  it("exec for stderr", () => {
    cy.exec("sh shell/test.sh", { failOnNonZeroExit: false }).then((obj) => {
      expect(obj.code).to.eq(0)
      expect(obj.stdout).to.contain("demo")

      cy.log(`${obj.stderr} ---`)
      assert(obj.stderr.length != 0)
      expect(obj.stderr).contain(
        "Using a password on the command line interface can be insecure."
      )
    })
  })
})

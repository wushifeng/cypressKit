// npx cypress run -s cypress/integration/mysql.spec.ts

it("mysql", () => {
  cy.task("dbexecute", "truncate table TABLE1")
  cy.task("dbquery", {
    query: " insert into TABLE1 (COLUMN1) values(?)",
    values: "bbb",
  })

  var q = "select * from TABLE1"
  cy.task("dbexecute", q).then((rows) => {
    cy.log(`db done: ${JSON.stringify(rows)}`)

    let data = JSON.stringify(rows)
    var json = JSON.parse(data)
    console.log(json[0].COLUMN1)
  })

  cy.task("dbexecute", q).then((rows) => {
    console.log(JSON.stringify(rows))
  })

  ///table2
  cy.task("dbexecute", "create table table2(col1 int, col2 varchar(100))")
  //数据库不要拼接字符串，防止注入
  cy.task("dbquery", {
    query: " insert into table2 (col1,col2) values(?, ?)",
    values: [1, "bbb"],
  })
  cy.task("dbexecute", "select * from table2").then((rows) => {
    console.log(JSON.stringify(rows))
  })
  cy.task("dbexecute", "drop table table2")

  cy.task("log", "hello")
})

/// <reference types="cypress" />

context("Demo", () => {
  ///
  before(() => {
    cy.log(`当前环境变量为${JSON.stringify(Cypress.env())}`);
    //    cy.log(`当前配置项信息为${JSON.stringify(Cypress.config())}`)
    cy.log("before");
    console.debug("before");

    cy.visit("/repository/npm/");
    cy.title().should("contain", "Repository - Nexus Repository Manager");
  });
  after(() => {
    console.debug("after");
    cy.log("after");
  });

  beforeEach(() => {
    console.debug("beforeEach");
    cy.log("beforeEach");
  });

  afterEach(() => {
    console.debug("afterEach");
    cy.log("afterEach");
  });

  ///
  describe("Group1", () => {
    it("t1- test case 1", () => {
      console.debug("t1");
      cy.log("t1");
      cy.get("a").should("contain", "HTML index");

      cy.get(".content-section").within(() => {
        cy.get("a:first").should("contain", "browse");
        cy.get("p:first").should("have.css", "display", "block");
        cy.get("p:last").should("have.css", "display", "block");
        //index start from 1
        cy.get("p:nth-child(2)").should("have.css", "display", "block");
      });

      cy.get(".nexus-body>.content-header img").should("match", "img");
    });

    it("t2- test case 2", () => {
      console.debug("t2");
      cy.log("t2");
    });
  });

  describe("Group2 ", () => {
    it("t3- test case 3", () => {
      console.debug("t2");
      cy.log("t3");

      cy.get(".nexus-body a")
        .eq(1)
        .then(($a) => {
          //$a.click()
          cy.wrap($a).click();
        });
    });
  });

  it("no group", () => {
    cy.log("t4");
  });
});

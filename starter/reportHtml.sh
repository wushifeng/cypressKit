#!/bin/bash

echo "delete last result files"
rm -rf cypress/videos/*
rm -rf cypress/screenshots/*
rm -rf cypress/results/*
rm -rf cypress/mochawesome-report/*
rm -f cypress/mochawesome.json

echo "run to generate all result"
#run cypress use mochawesome reporter
#./node_modules/cypress/bin/cypress run --reporter mocha-multi-reporters --reporter-options configFile=reporter-config.json
#./node_modules/cypress/bin/cypress run --reporter mochawesome --reporter-options "reportDir=cypress/results,overwrite=false,html=false,json=true"
# npx cypress run
npx cypress run --reporter mochawesome --reporter-options "reportDir=cypress/results,overwrite=false,html=false,json=true"

echo "generate report can view in browser"
npx mochawesome-merge -o ./cypress/mochawesome.json ./cypress/results/*
npx mochawesome-report-generator ./cypress/mochawesome.json -o ./mochawesome-report

echo "done!"
